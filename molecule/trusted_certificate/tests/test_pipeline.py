import os
import json
import time

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("gitlab_server")


def test_runner_job_success(host):
    access_token = host.run("cat /tmp/access_token").stdout
    project_url = "https://localhost/api/v4/projects/2"

    pipeline = json.loads(
        host.run(
            f"curl -s -X POST --header 'PRIVATE-TOKEN: {access_token}' "
            f"{project_url}/pipeline?ref=master"
        ).stdout
    )

    result = "pending"
    while result == "pending" or result == "running":

        pipeline_result = json.loads(
            host.run(
                f"curl -s -X GET --header 'PRIVATE-TOKEN: {access_token}' "
                f"{project_url}/pipelines/{pipeline['id']}"
            ).stdout
        )
        result = pipeline_result["status"]
        time.sleep(5)

    assert pipeline_result["status"] == "success"
