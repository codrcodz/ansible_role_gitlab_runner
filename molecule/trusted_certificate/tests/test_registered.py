import os
import pytest

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("gitlab_runner")


@pytest.mark.parametrize("package", [
    "docker-ce",
    "gitlab-runner"
])
def test_packages_installed(host, package):

    assert host.package(package).is_installed


def test_runner_registered(host):
    runner_status = host.run("gitlab-runner verify")

    assert runner_status.rc == 0
